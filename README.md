# OpenML dataset: Development-Category-(10k-courses)-from-Udemy

https://www.openml.org/d/43767

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
A compilation of all the development related courses ( 10 thousand courses) which are available on Udemy's website. Under the development category, there are courses from Web Development, Data Science, Mobile App, Programming Languages, Game Development, Databases, Software Testing, Software Engineering, Development Tools and E-commerce each having multiple courses under it's domain.
All the details can be found on Udemy's website as well!
Content
Here, I have extracted data related to 10k courses which come under the development category on Udemy's website.
The 17 columns in the dataset can be used to gain insights related to:

id : The course ID of that particular course.
title : Shows the unique names of the courses available under the development category on Udemy.
url: Gives the URL of the course.
is_paid : Returns a boolean value displaying true if the course is paid and false if otherwise.
num_subscribers : Shows the number of people who have subscribed that course.
avg_rating : Shows the average rating of the course.
avg rating recent : Reflects the recent changes in the average rating.
num_reviews : Gives us an idea related to the number of ratings that a course has received.
num_ published_lectures : Shows the number of lectures the course offers.
num_ published_ practice_tests : Gives an idea of the number of practice tests that a course offers.
created :  The time of creation of the course.
published_time : Time of publishing the course.
discounted_ price_amount :  The discounted price which a certain course is being offered at.
discounted_ price_currency :  The currency corresponding to the discounted price which a certain course is being offered at.
price_ detail_amount : The original price of a particular course.
price_ detail_currency : The currency corresponding to the price detail amount for a course.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43767) of an [OpenML dataset](https://www.openml.org/d/43767). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43767/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43767/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43767/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

